import 'dart:io';

import 'package:permission_handler/permission_handler.dart';

import 'grant_permission_strategy.dart';

class GrantPermissionCameraStrategy extends GrantPermissionStrategy {
  GrantPermissionCameraStrategy() : super(Permission.camera);
}

class GrantPermissionPhotosStrategy extends GrantPermissionStrategy {
  GrantPermissionPhotosStrategy()
      : super(Platform.isAndroid ? Permission.storage : Permission.photos);
}
