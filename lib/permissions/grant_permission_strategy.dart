import 'package:permission_handler/permission_handler.dart';

/// handles .isLimited for iOS 14+ where we can restrict access.
abstract class GrantPermissionStrategy {
  final Permission permission;

  GrantPermissionStrategy(this.permission);

  Future<void> request({
    required final OnPermatentlyDenied onPermatentlyDenied,
    required final OnGranted onGranted,
  }) async {
    var status = await permission.status;

    if (status.isPermanentlyDenied) {
      onPermatentlyDenied.call();
      return;
    }
    if (!status.isLimited && !status.isGranted) {
      final result = await permission.request();
      if (!result.isGranted) {
        return;
      }
    }
    onGranted.call();
  }
}

typedef OnPermatentlyDenied = void Function();

typedef OnGranted = void Function();
