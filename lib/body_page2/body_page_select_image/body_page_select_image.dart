import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:image_picker/image_picker.dart';
import 'package:partes_do_corpo_teste/body_page2/body_page_v2.dart';
import 'package:partes_do_corpo_teste/permissions/grant_permission_concrete.dart';
import 'package:system_settings/system_settings.dart';

import '../controller_body_page.dart';

class BodyPageSelectImage extends StatefulWidget{
  const BodyPageSelectImage({Key? key}) : super(key: key);

  final imageFront = 'assets/images/img_body_front.png';

  @override
  State<StatefulWidget> createState() => _BodyPageSelectImageState();

}

class _BodyPageSelectImageState extends State<BodyPageSelectImage>{
  @override
  Widget build(BuildContext context) {

    SchedulerBinding.instance?.addPostFrameCallback((timeStamp) {
      Future.delayed(
          const Duration(milliseconds: 1000),
              (){
            showModalBottomSheetAskOriginImage(
                context,
                    (xfile){

                      if(xfile != null){

                        Navigator.pushReplacement(
                            context, MaterialPageRoute(
                            builder: (context) =>
                                  BodyPageV2(
                                        title: 'Partes do corpo',
                                        controllerBodyPage: ControllerBodyPage(),
                                        xfile: xfile,
                                    )));
                          }else{
                          Navigator.of(context).pop();
                        }
                      }
                    );
          });
    });

    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.fill,
          image: AssetImage(
              widget.imageFront,
          ),
        ),
      ),
    );
  }

  Future<dynamic> showModalBottomSheetAskOriginImage(BuildContext context, Function(XFile?) callback) {
    return showModalBottomSheet(
        isDismissible: false,
        enableDrag: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
        builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              Navigator.of(context).pop();
              return false;
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 48.0),
              child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.all(32.0),
                  child: Text(
                    'De onde deseja adicionar a foto?',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        FloatingActionButton(
                            onPressed: () async {
                              await GrantPermissionPhotosStrategy().request(
                                  onPermatentlyDenied: () {
                                    ScaffoldMessenger.of(context).showSnackBar(snackBar());
                                  }, onGranted: () async {
                                //Retorna imagem selecionada
                                callback(await _imgFromCamera());
                              });
                              Navigator.of(context).pop();
                            },
                          child: const Icon(Icons.photo_camera),
                        ),
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('Câmera'),
                        )
                      ],
                    ),

                    Column(
                      children: [
                        FloatingActionButton(
                          onPressed: () async {
                            await GrantPermissionPhotosStrategy().request(
                                onPermatentlyDenied: () {
                                  ScaffoldMessenger.of(context).showSnackBar(snackBar());
                                }, onGranted: () async {
                              //Retorna imagem selecionada
                              callback(await _imgFromGallery());
                            });
                            Navigator.of(context).pop();
                          },
                          child: const Icon(Icons.photo),
                        ),
                        const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('Galeria'),
                        )
                      ],
                    ),
                  ],
                ),
              ],
              ),
            ),
          );
        });
  }
}

Future<XFile?> _imgFromCamera() async {
  return await ImagePicker()
      .pickImage(source: ImageSource.camera, imageQuality: 100);
}

Future<XFile?> _imgFromGallery() async {
  return await ImagePicker()
      .pickImage(source: ImageSource.gallery, imageQuality: 100);
}

SnackBar snackBar() {
  return const SnackBar(
    duration: Duration(days: 1),
    backgroundColor: Colors.redAccent,
    content: Text('É necessário permitir acesso pela configuração do app'),
    action: SnackBarAction(
      label: 'Ok',
      onPressed: SystemSettings.app,
    ),
  );
}




















