import 'package:partes_do_corpo_teste/body_page2/partes/body.dart';
import 'package:partes_do_corpo_teste/body_page2/partes/partes.dart';
import 'package:partes_do_corpo_teste/body_page2/tags/tags.dart';

const tatuagemTag = 'tatuagem_tag';
const cicatrizTag = 'cicatriz_tag';
const outrosTag = 'outros_tag';

class ControllerBodyPage {

  Body body = Body();
  final List<ParteMacro> _listPartesSeleted = [];

 // final Map<String, Tags> mapTags = {tatuagemTag : TatuagemTag(), cicatrizTag : CicatrizTag(), outrosTag : OutrosTag()};
  final List<Tags> listTags = [TatuagemTag(), CicatrizTag(), OutrosTag()];

  final List<Tags> _listTagsSelected = [];

  var frontal = true;

  int getLengthListTagSelected (){
    return _listTagsSelected.length;
  }

  void addTag(Tags tag){
    tag.isSelected = true;
    _listTagsSelected.add(tag);
    //Todo: Verificar se essa lógica é realmente a mais adequada (Está igual ao nativo)
  }

  String getPartSelected(){

    switch(_listPartesSeleted.length) {
      case 0:
        return ParteMensagem.mensagemDefault;
      case 1:
        return _getMensagemParte();
      default:
        return ParteMensagem.mensagemMultiplasPartes;

    }
  }

  String _getMensagemParte(){
    final part = _listPartesSeleted.first;

    switch(_listPartesSeleted.length) {
      case 0:
        return ParteMensagem.mensagemDefault;
      case 1:
        if(part.getListMicroLength() == 1){
          if(part.getListUnidadeLength() == 1){
            return part.getMensagemUnidade();
          }
          return part.getMensagemMicro();
        }
          return part.getMensagem();

      default:
        return ParteMensagem.mensagemMultiplasPartes;
    }

  }



  void changeSelect(ParteMacro parteMacro, ParteMicro parteMicro, ParteUnitaria parteUnitaria) {

    if(parteUnitaria.getAndInvertSelection()) {

      parteMicro.removeParte(parteUnitaria);

      if(parteMicro.getListUnidadeLength() == 0){
        parteMacro.removeParteMicro(parteMicro);
        if(parteMacro.getListMicroLength() == 0) {
          _listPartesSeleted.remove(parteMacro);
        }
      }

    }else{
      if(parteMicro.getListUnidadeLength() == 0) {
        if(parteMacro.getListMicroLength() == 0){
          _listPartesSeleted.add(parteMacro);
        }
        parteMacro.addParteMicro(parteMicro);
      }
      parteMicro.addParte(parteUnitaria);
    }

  }

}




































