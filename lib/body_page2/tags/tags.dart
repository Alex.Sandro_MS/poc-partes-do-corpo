abstract class Tags{
  final String descricao;
  final int cod;
  var isSelected = false;

  Tags(this.descricao, this.cod);
}

class DefaultTag extends Tags{
  DefaultTag() : super('Escolha', -1);
}

class TatuagemTag extends Tags{
  TatuagemTag() : super('Tatuagem', 1);
}

class CicatrizTag extends Tags{
  CicatrizTag() : super('Cicatriz', 2);
}

class OutrosTag extends Tags{
  OutrosTag() : super('Outros', 3);
}