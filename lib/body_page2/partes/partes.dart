abstract class ParteMacro implements ParteMensagem {
  //Corpo inteiro
  void addParteMicro(ParteMicro parteMicro);
  void removeParteMicro(ParteMicro parteMicro);

  String getMensagemUnidade();
  int getListUnidadeLength();

  String getMensagemMicro();
  int getListMicroLength();
}

abstract class ParteMicro implements ParteMensagem {
  void addParte(ParteUnitaria parteUnitaria);
  void removeParte(ParteUnitaria parteUnitaria);
  String getMensagemUnidade();
  int getListUnidadeLength();
//Conjunto de Partes unitarias
}


abstract class ParteUnitaria implements ParteMensagem{
  final int cod;
  bool isSelected = false;

  bool getAndInvertSelection(){
    isSelected = !isSelected;
    return !isSelected;
  }

  ParteUnitaria(this.cod);
}

abstract class ParteMensagem{
  static const String mensagemDefault = 'Toque na Área a Classificar';
  static const String mensagemMultiplasPartes = 'Múltiplas Partes';

  String getMensagem();
  int getCodeParte();
}

abstract class CodeParteCorpo{
  static const int cabecaDireita        = 1;
  static const int cabecaEsquerda       = 2;
  static const int pescocoDireito       = 3;
  static const int pescocoEsquerdo      = 4;
  static const int troncoSupDireito     = 5;
  static const int troncoSupEsquerdo    = 6;
  static const int troncoInfDireito     = 7;
  static const int troncoInfEsquerdo    = 8;
  static const int bracoDireito         = 9;
  static const int bracoEsquerdo        = 10;
  static const int antebracoDireito     = 11;
  static const int antebracoEsquerdo    = 12;
  static const int maoDireita           = 13;
  static const int maoEsquerda          = 14;
  static const int coxaDireita          = 15;
  static const int coxaEsquerda         = 16;
  static const int joelhoDireito        = 17;
  static const int joelhoEsquerdo       = 18;
  static const int pernaDireita         = 19;
  static const int pernaEsquerda        = 20;
  static const int peDireito            = 21;
  static const int peEsquerdo           = 22;
  static const int multiplasPartes      = 23;
  static const int cabecaInteira        = 24;
  static const int pescocoInteiro       = 25;
  static const int bracoDireitoInteiro  = 26;
  static const int bracoEsquerdoInteiro = 27;
  static const int troncoInteiro        = 28;
  static const int pernaDireitaInteira  = 29;
  static const int pernaEsquerdaInteira = 30;
}
