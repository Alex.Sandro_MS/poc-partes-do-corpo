import 'package:partes_do_corpo_teste/body_page2/partes/partes.dart';


class PernaDireitaMacro extends ParteMacro{
  final List<ParteMicro> _listMicroParteSelected = [];
  PernaDireitaMicro pernaDireitaMicro = PernaDireitaMicro();

  @override
  String getMensagem() {
    return pernaDireitaMicro.getMensagem();
  }

  @override
  void addParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.add(parteMicro);
  }

  @override
  void removeParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.remove(parteMicro);
  }

  @override
  String getMensagemUnidade() {
    return _listMicroParteSelected.first.getMensagemUnidade();
  }

  @override
  int getListMicroLength() {
    return _listMicroParteSelected.length;
  }

  @override
  String getMensagemMicro() {
    return _listMicroParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listMicroParteSelected.first.getListUnidadeLength();
  }

  @override
  int getCodeParte() {
      return CodeParteCorpo.pernaDireitaInteira;
  }
}


class PernaDireitaMicro extends ParteMicro{
  final List<ParteUnitaria> _listParteSelected = [];
  CoxaDireita coxaDireita = CoxaDireita();
  PernaDireita pernaDireita = PernaDireita();
  JoelhoDireito joelhoDireito = JoelhoDireito();
  PeDireito peDireito = PeDireito();

  @override
  String getMensagem() {
    return 'Perna Direita Inteira';
  }

  @override
  void addParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.add(parteUnitaria);
  }

  @override
  void removeParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.remove(parteUnitaria);
  }

  @override
  String getMensagemUnidade() {
   return _listParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
   return _listParteSelected.length;
  }

  @override
  int getCodeParte() {
        return CodeParteCorpo.pernaDireitaInteira;
  }
}


class CoxaDireita extends ParteUnitaria{
  CoxaDireita() : super(CodeParteCorpo.coxaDireita);


  @override
  String getMensagem() {
    return 'Coxa Direita';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}

class JoelhoDireito extends ParteUnitaria{
  JoelhoDireito() : super(CodeParteCorpo.joelhoDireito);


  @override
  String getMensagem() {
    return 'Joelho Direito';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}

class PernaDireita extends ParteUnitaria{
  PernaDireita() : super(CodeParteCorpo.pernaDireita);


  @override
  String getMensagem() {
    return 'Perna Direita';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}

class PeDireito extends ParteUnitaria{
  PeDireito() : super(CodeParteCorpo.peDireito);


  @override
  String getMensagem() {
    return 'Pé Direito';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}