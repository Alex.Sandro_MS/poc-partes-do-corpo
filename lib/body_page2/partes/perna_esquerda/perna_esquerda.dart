import 'package:partes_do_corpo_teste/body_page2/partes/partes.dart';


class PernaEsquerdaMacro extends ParteMacro{
  final List<ParteMicro> _listMicroParteSelected = [];
  PernaEsquerdaMicro pernaEsquerdaMicro = PernaEsquerdaMicro();

  @override
  String getMensagem() {
    return pernaEsquerdaMicro.getMensagem();
  }

  @override
  void addParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.add(parteMicro);
  }

  @override
  void removeParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.remove(parteMicro);
  }

  @override
  String getMensagemUnidade() {
    return _listMicroParteSelected.first.getMensagemUnidade();
  }

  @override
  int getListMicroLength() {
    return _listMicroParteSelected.length;
  }

  @override
  String getMensagemMicro() {
    return _listMicroParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listMicroParteSelected.first.getListUnidadeLength();
  }

  @override
  int getCodeParte() {
    return CodeParteCorpo.pernaEsquerdaInteira;
  }
}


class PernaEsquerdaMicro extends ParteMicro{
  final List<ParteUnitaria> _listParteSelected = [];
  CoxaEsquerda coxaEsquerda = CoxaEsquerda();
  PernaEsquerda pernaEsquerda = PernaEsquerda();
  JoelhoEsquerdo joelhoEsquerdo = JoelhoEsquerdo();
  PeEsquerdo peEsquerdo = PeEsquerdo();

  @override
  String getMensagem() {
    return 'Perna Esquerda Inteira';
  }

  @override
  void addParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.add(parteUnitaria);
  }

  @override
  void removeParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.remove(parteUnitaria);
  }

  @override
  String getMensagemUnidade() {
   return _listParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
   return _listParteSelected.length;
  }

  @override
  int getCodeParte() {
   return CodeParteCorpo.pernaEsquerdaInteira;
  }
}


class CoxaEsquerda extends ParteUnitaria{
  CoxaEsquerda() : super(CodeParteCorpo.coxaEsquerda);


  @override
  String getMensagem() {
    return 'Coxa Esquerda';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}

class JoelhoEsquerdo extends ParteUnitaria{
  JoelhoEsquerdo() : super(CodeParteCorpo.joelhoEsquerdo);


  @override
  String getMensagem() {
    return 'Joelho Esquerdo';
  }



  @override
  int getCodeParte() {
    return cod;
  }
}

class PernaEsquerda extends ParteUnitaria{
  PernaEsquerda() : super(CodeParteCorpo.pernaEsquerda);


  @override
  String getMensagem() {
    return 'Perna Esquerda';
  }



  @override
  int getCodeParte() {
    return cod;
  }
}

class PeEsquerdo extends ParteUnitaria{
  PeEsquerdo() : super(CodeParteCorpo.peEsquerdo);


  @override
  String getMensagem() {
    return 'Pé Esquerdo';
  }



  @override
  int getCodeParte() {
    return cod;
  }
}