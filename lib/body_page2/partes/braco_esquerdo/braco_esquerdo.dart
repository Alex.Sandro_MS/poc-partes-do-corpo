import 'package:partes_do_corpo_teste/body_page2/partes/partes.dart';


class BracoEsquerdoMacro extends ParteMacro{
  final List<ParteMicro> _listMicroParteSelected = [];
  BracoEsquerdoMicro bracoEsquerdoMicro = BracoEsquerdoMicro();

  @override
  String getMensagem() {
    return bracoEsquerdoMicro.getMensagem();
  }

  @override
  void addParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.add(parteMicro);
  }

  @override
  void removeParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.remove(parteMicro);
  }

  @override
  String getMensagemUnidade() {
    return _listMicroParteSelected.first.getMensagemUnidade();
  }

  @override
  int getListMicroLength() {
    return _listMicroParteSelected.length;
  }

  @override
  String getMensagemMicro() {
    return _listMicroParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listMicroParteSelected.first.getListUnidadeLength();
  }

  @override
  int getCodeParte() {
    return CodeParteCorpo.bracoEsquerdoInteiro;
  }
}


class BracoEsquerdoMicro extends ParteMicro{
  final List<ParteUnitaria> _listParteSelected = [];
  AntebracoEsquerdo antebracoEsquerdo = AntebracoEsquerdo();
  BracoEsquerdo bracoEsquerdo = BracoEsquerdo();
  MaoEsquerda maoEsquerda = MaoEsquerda();

  @override
  String getMensagem() {
    return 'Braço Esquerdo Inteiro';
  }

  @override
  void addParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.add(parteUnitaria);
  }

  @override
  void removeParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.remove(parteUnitaria);
  }

  @override
  String getMensagemUnidade() {
   return _listParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
   return _listParteSelected.length;
  }

  @override
  int getCodeParte() {
    return CodeParteCorpo.bracoEsquerdoInteiro;
  }



}

class BracoEsquerdo extends ParteUnitaria{
  BracoEsquerdo() : super(CodeParteCorpo.bracoEsquerdo);


  @override
  String getMensagem() {
    return 'Braço Esquerdo';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}

class AntebracoEsquerdo extends ParteUnitaria{
  AntebracoEsquerdo() : super(CodeParteCorpo.antebracoEsquerdo);


  @override
  String getMensagem() {
    return 'Antebraço Esquerdo';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}

class MaoEsquerda extends ParteUnitaria{
  MaoEsquerda() : super(CodeParteCorpo.maoEsquerda);


  @override
  String getMensagem() {
    return 'Mão Esquerda';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}