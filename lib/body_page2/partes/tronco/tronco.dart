import 'package:partes_do_corpo_teste/body_page2/partes/partes.dart';

///---MACRO
class TroncoMacro extends ParteMacro{
  final List<ParteMicro> _listMicroParteSelected = [];
  TroncoSupMicro troncoSupMicro = TroncoSupMicro();
  TroncoInfMicro troncoInfMicro = TroncoInfMicro();

  @override
  String getMensagem() {
    return 'Tronco Inteiro';
  }

  @override
  void addParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.add(parteMicro);
  }

  @override
  void removeParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.remove(parteMicro);
  }

  @override
  String getMensagemUnidade() {
    return _listMicroParteSelected.first.getMensagemUnidade();
  }

  @override
  int getListMicroLength() {
    return _listMicroParteSelected.length;
  }

  @override
  String getMensagemMicro() {
    return _listMicroParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listMicroParteSelected.first.getListUnidadeLength();
  }

  @override
  int getCodeParte() {
    return CodeParteCorpo.troncoInteiro;
  }
}


///---MICRO Sup
class TroncoSupMicro extends ParteMicro{
  final List<ParteUnitaria> _listMicroParteSelected = [];
  TroncoSuperiorDireito troncoSuperiorDireito = TroncoSuperiorDireito();
  TroncoSuperiorEsquerdo troncoSuperiorEsquerdo = TroncoSuperiorEsquerdo();

  @override
  String getMensagem() {
    return 'Tronco Inteiro';
  }

  @override
  void addParte(ParteUnitaria parteUnitaria) {
    _listMicroParteSelected.add(parteUnitaria);
  }

  @override
  void removeParte(ParteUnitaria parteUnitaria) {
    _listMicroParteSelected.remove(parteUnitaria);
  }

  @override
  String getMensagemUnidade() {
   return _listMicroParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
   return _listMicroParteSelected.length;
  }


  @override
  int getCodeParte() {
    return CodeParteCorpo.troncoInteiro;
  }
}




///---MICRO Inf
class TroncoInfMicro extends ParteMicro{
  final List<ParteUnitaria> _listParteSelected = [];
  TroncoInferiorDireito troncoInferiorDireito = TroncoInferiorDireito();
  TroncoInferiorEsquerdo troncoInferiorEsquerdo = TroncoInferiorEsquerdo();

  @override
  String getMensagem() {
    return 'Tronco Inteiro';
  }

  @override
  void addParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.add(parteUnitaria);
  }

  @override
  void removeParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.remove(parteUnitaria);
  }

  @override
  String getMensagemUnidade() {
    return _listParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listParteSelected.length;
  }


  @override
  int getCodeParte() {
    return CodeParteCorpo.troncoInteiro;
  }
}



///---UNIT
class TroncoSuperiorDireito extends ParteUnitaria{
  TroncoSuperiorDireito() : super(CodeParteCorpo.troncoSupDireito);


  @override
  String getMensagem() {
    return 'Tronco Superior Direito';
  }

  @override
  int getCodeParte() {
    return cod;
  }


}


class TroncoSuperiorEsquerdo extends ParteUnitaria{
  TroncoSuperiorEsquerdo() : super(CodeParteCorpo.troncoSupEsquerdo);


  @override
  String getMensagem() {
    return 'Tronco Superior Esquerdo';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}

class TroncoInferiorDireito extends ParteUnitaria{
  TroncoInferiorDireito() : super(CodeParteCorpo.troncoInfDireito);


  @override
  String getMensagem() {
    return 'Tronco Inferior Direito';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}

class TroncoInferiorEsquerdo extends ParteUnitaria{
  TroncoInferiorEsquerdo() : super(CodeParteCorpo.troncoInfEsquerdo);


  @override
  String getMensagem() {
    return 'Tronco Inferior Esquerdo';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}