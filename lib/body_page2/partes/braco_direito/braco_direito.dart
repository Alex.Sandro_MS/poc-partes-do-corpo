import 'package:partes_do_corpo_teste/body_page2/partes/partes.dart';

class BracoDireitoMacro extends ParteMacro{
  final List<ParteMicro> _listMicroParteSelected = [];
  BracoDireitoMicro bracoDireitoMicro = BracoDireitoMicro();

  @override
  String getMensagem() {
    return bracoDireitoMicro.getMensagem();
  }

  @override
  void addParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.add(parteMicro);
  }

  @override
  void removeParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.remove(parteMicro);
  }

  @override
  String getMensagemUnidade() {
    return _listMicroParteSelected.first.getMensagemUnidade();
  }

  @override
  int getListMicroLength() {
    return _listMicroParteSelected.length;
  }

  @override
  String getMensagemMicro() {
    return _listMicroParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listMicroParteSelected.first.getListUnidadeLength();
  }

  @override
  int getCodeParte() {
    return bracoDireitoMicro.getCodeParte();
  }
}


class BracoDireitoMicro extends ParteMicro{
  final List<ParteUnitaria> _listParteSelected = [];
  AntebracoDireito antebracoDireito = AntebracoDireito();
  BracoDireito bracoDireito = BracoDireito();
  MaoDireita maoDireita = MaoDireita();

  @override
  String getMensagem() {
    return 'Braço Direito Inteiro';
  }

  @override
  void addParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.add(parteUnitaria);
  }

  @override
  void removeParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.remove(parteUnitaria);
  }

  @override
  String getMensagemUnidade() {
   return _listParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
   return _listParteSelected.length;
  }

  @override
  int getCodeParte() {
    return CodeParteCorpo.bracoDireitoInteiro;
  }



}

class BracoDireito extends ParteUnitaria{
  BracoDireito() : super(CodeParteCorpo.bracoDireito);


  @override
  String getMensagem() {
    return 'Braço Direito';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}

class AntebracoDireito extends ParteUnitaria{
  AntebracoDireito() : super(CodeParteCorpo.antebracoDireito);


  @override
  String getMensagem() {
    return 'Antebraço Direito';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}

class MaoDireita extends ParteUnitaria{
  MaoDireita() : super(CodeParteCorpo.maoDireita);


  @override
  String getMensagem() {
    return 'Mão Direita';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}