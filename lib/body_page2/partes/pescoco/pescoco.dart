import 'package:partes_do_corpo_teste/body_page2/partes/partes.dart';


class PescocoMacro extends ParteMacro{
  final List<ParteMicro> _listMicroParteSelected = [];
  PescocoMicro pescocoMicro = PescocoMicro();

  @override
  String getMensagem() {
    return pescocoMicro.getMensagem();
  }

  @override
  void addParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.add(parteMicro);
  }

  @override
  void removeParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.remove(parteMicro);
  }

  @override
  String getMensagemUnidade() {
    return _listMicroParteSelected.first.getMensagemUnidade();
  }

  @override
  int getListMicroLength() {
    return _listMicroParteSelected.length;
  }

  @override
  String getMensagemMicro() {
    return _listMicroParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listMicroParteSelected.first.getListUnidadeLength();
  }


  @override
  int getCodeParte() {
    return CodeParteCorpo.pescocoInteiro;
  }
}



class PescocoMicro extends ParteMicro{
  final List<ParteUnitaria> _listParteSelected = [];
  PescocoDireito pescocoDireito = PescocoDireito();
  PescocoEsquerdo pescocoEsquerdo = PescocoEsquerdo();

  @override
  String getMensagem() {
    return 'Pescoço Inteiro';
  }

  @override
  void addParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.add(parteUnitaria);
  }

  @override
  void removeParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.remove(parteUnitaria);
  }

  @override
  String getMensagemUnidade() {
    return _listParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listParteSelected.length;
  }


  @override
  int getCodeParte() {
    return CodeParteCorpo.pescocoInteiro;
  }
}

class PescocoDireito extends ParteUnitaria{
  PescocoDireito() : super(CodeParteCorpo.pescocoDireito);


  @override
  String getMensagem() {
    return 'Pescoço Direito';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}

class PescocoEsquerdo extends ParteUnitaria{
  PescocoEsquerdo() : super(CodeParteCorpo.pescocoDireito);


  @override
  String getMensagem() {
    return 'Pescoço Esquerdo';
  }

  @override
  int getCodeParte() {
    return cod;
  }
}


