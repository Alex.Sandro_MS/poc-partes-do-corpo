import 'package:partes_do_corpo_teste/body_page2/partes/partes.dart';


class CabecaMacro extends ParteMacro{
  final List<ParteMicro> _listMicroParteSelected = [];
  CabecaMicro cabecaMicro = CabecaMicro();

  @override
  String getMensagem() {
    return cabecaMicro.getMensagem();
  }

  @override
  void addParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.add(parteMicro);
  }

  @override
  void removeParteMicro(ParteMicro parteMicro) {
    _listMicroParteSelected.remove(parteMicro);
  }

  @override
  String getMensagemUnidade() {
    return _listMicroParteSelected.first.getMensagemUnidade();
  }

  @override
  int getListMicroLength() {
    return _listMicroParteSelected.length;
  }

  @override
  String getMensagemMicro() {
    return _listMicroParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listMicroParteSelected.first.getListUnidadeLength();
  }

  @override
  int getCodeParte() {
    return CodeParteCorpo.cabecaInteira;
  }
}

class CabecaMicro extends ParteMicro{
  final List<ParteUnitaria> _listParteSelected = [];
  CabecaDireita cabecaDireita = CabecaDireita();
  CabecaEsquerda cabecaEsquerda = CabecaEsquerda();

  @override
  String getMensagem() {
    return 'Cabeça Inteira';
  }

  @override
  void addParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.add(parteUnitaria);
  }

  @override
  void removeParte(ParteUnitaria parteUnitaria) {
    _listParteSelected.remove(parteUnitaria);
  }

  @override
  String getMensagemUnidade() {
    return _listParteSelected.first.getMensagem();
  }

  @override
  int getListUnidadeLength() {
    return _listParteSelected.length;
  }

  @override
  int getCodeParte() {
    return CodeParteCorpo.cabecaInteira;
  }
}



class CabecaDireita extends ParteUnitaria{
  CabecaDireita() : super(CodeParteCorpo.cabecaDireita);


  @override
  String getMensagem() {
    return 'Cabeça Direita';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}

class CabecaEsquerda extends ParteUnitaria{
  CabecaEsquerda() : super(CodeParteCorpo.cabecaEsquerda);


  @override
  String getMensagem() {
    return 'Cabeça Esquerda';
  }


  @override
  int getCodeParte() {
    return cod;
  }
}


