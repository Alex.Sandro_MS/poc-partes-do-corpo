import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:partes_do_corpo_teste/body_page2/controller_body_page.dart';
import 'package:partes_do_corpo_teste/body_page2/tags/tags.dart';

enum BodyParts {
  leftArm, head, torso, leftLeg, rightLeg, rightArm
}

class BodyPageV2 extends StatefulWidget {
  final String title;
  final ControllerBodyPage controllerBodyPage;
  final XFile xfile;

  const BodyPageV2({Key? key, required this.title, required this.controllerBodyPage, required this.xfile}) : super(key: key);
  
  final seletionColor = const Color(0x7C009EFD);
  final seletionBorderColor = const Color(0xFF033F7B);

  final imageFront = 'assets/images/img_body_front.png';
  final imageBack = 'assets/images/img_body_back.png';

  @override
  State<BodyPageV2> createState() => _BodyPageV2State();
}

class _BodyPageV2State extends State<BodyPageV2> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: const Icon(
              Icons.cloud_upload_sharp,
            ),
            onPressed: (){
              showModalBottomSheetSelectTag(
                context
              );

            },
          ),

        ],
      ),
      body: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,

          children: [
            ElevatedButton(
                style: !widget.controllerBodyPage.frontal
                    ? ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.grey))
                    : ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.deepPurple)),
                onPressed: (){
                  setState(() {
                    widget.controllerBodyPage.frontal = true;
                  });
                },
                child: const Text('Frontal')
            ),

            ElevatedButton(
              style: widget.controllerBodyPage.frontal
                  ? ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.grey))
                  : ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.deepPurple)),
                onPressed: (){
                  setState(() {
                    widget.controllerBodyPage.frontal = false;
                  });
                },
                child: const Text('Dorsal')
            )
          ],
        ),
        Flexible(
          child: Stack(
              children:[
                Card(
                  margin: EdgeInsets.zero,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(
                          widget.controllerBodyPage.frontal
                              ? widget.imageFront
                              : widget.imageBack,
                        ),
                      ),
                    ),
                  ),
                ),

                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Image.file(
                              File(widget.xfile.path),
                            //  width: 90.0,
                              height: 90.0,
                              fit: BoxFit.scaleDown,
                            ),
                      ),
                    ),
                  ],
                ),

                Column(
                  children: [

                    //Cabeça
                    Flexible(
                      flex: 11,
                      child: Row(
                        children: [

                          //Cabeça direita
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(135.0, 0.0, 0.0, 0.0,),
                              child: InkWell(
                                  onTap: (){
                                    setState(() {
                                      if(widget.controllerBodyPage.frontal) {
                                        widget.controllerBodyPage
                                            .changeSelect(
                                            widget.controllerBodyPage.body.cabeca,
                                            widget.controllerBodyPage.body.cabeca.cabecaMicro,
                                            widget.controllerBodyPage.body.cabeca.cabecaMicro.cabecaDireita);
                                      }else{
                                        widget.controllerBodyPage
                                            .changeSelect(
                                            widget.controllerBodyPage.body.cabeca,
                                            widget.controllerBodyPage.body.cabeca.cabecaMicro,
                                            widget.controllerBodyPage.body.cabeca.cabecaMicro.cabecaEsquerda);
                                      }
                                    });
                                  },
                                  child: BodySelectContainer(
                                      frontal: widget.controllerBodyPage.frontal,
                                      botaoFrontL: widget.controllerBodyPage.body.cabeca.cabecaMicro.cabecaDireita.isSelected,
                                      botaoFrontR: widget.controllerBodyPage.body.cabeca.cabecaMicro.cabecaEsquerda.isSelected
                                  )
                              ),

                            ),
                          ),

                          //Cabeça esquerda
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 135.0, 0.0,),
                              child: InkWell(
                                  onTap: (){
                                    setState(() {
                                      if(widget.controllerBodyPage.frontal) {
                                        widget.controllerBodyPage
                                            .changeSelect(
                                            widget.controllerBodyPage.body.cabeca,
                                            widget.controllerBodyPage.body.cabeca.cabecaMicro,
                                            widget.controllerBodyPage.body.cabeca.cabecaMicro.cabecaEsquerda);
                                      }else{
                                        widget.controllerBodyPage
                                            .changeSelect(
                                            widget.controllerBodyPage.body.cabeca,
                                            widget.controllerBodyPage.body.cabeca.cabecaMicro,
                                            widget.controllerBodyPage.body.cabeca.cabecaMicro.cabecaDireita);
                                      }
                                    });
                                  },
                                  child:  BodySelectContainer(
                                      frontal: widget.controllerBodyPage.frontal,
                                      botaoFrontL: widget.controllerBodyPage.body.cabeca.cabecaMicro.cabecaEsquerda.isSelected,
                                      botaoFrontR: widget.controllerBodyPage.body.cabeca.cabecaMicro.cabecaDireita.isSelected
                                  )
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),



                    //Pescoço
                    Flexible(
                      flex: 4,
                      child: Row(
                        children: [

                          //pesR
                          Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(135.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pescoco,
                                              widget.controllerBodyPage.body.pescoco.pescocoMicro,
                                              widget.controllerBodyPage.body.pescoco.pescocoMicro.pescocoDireito);
                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pescoco,
                                              widget.controllerBodyPage.body.pescoco.pescocoMicro,
                                              widget.controllerBodyPage.body.pescoco.pescocoMicro.pescocoEsquerdo);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pescoco.pescocoMicro.pescocoDireito.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pescoco.pescocoMicro.pescocoEsquerdo.isSelected
                                    )
                                ),
                              )
                          ),

                          //pesL
                          Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 135.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pescoco,
                                              widget.controllerBodyPage.body.pescoco.pescocoMicro,
                                              widget.controllerBodyPage.body.pescoco.pescocoMicro.pescocoEsquerdo);
                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pescoco,
                                              widget.controllerBodyPage.body.pescoco.pescocoMicro,
                                              widget.controllerBodyPage.body.pescoco.pescocoMicro.pescocoDireito);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pescoco.pescocoMicro.pescocoEsquerdo.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pescoco.pescocoMicro.pescocoDireito.isSelected
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),



                    //Tronco superior - Antebraço
                    Flexible(
                      flex: 14,
                      child: Row(
                        children: [

                          //AntBraR
                          Flexible(
                            flex: 10,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(80.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoDireito,
                                              widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro,
                                              widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.antebracoDireito);
                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoEsquerdo,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.antebracoEsquerdo);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.antebracoDireito.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.antebracoEsquerdo.isSelected
                                    )
                                ),
                              )
                          ),

                          //TroR
                          Flexible(
                            flex: 4,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.troncoMacro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoSupMicro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoSupMicro.troncoSuperiorDireito);
                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.troncoMacro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoSupMicro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoSupMicro.troncoSuperiorEsquerdo);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.troncoMacro.troncoSupMicro.troncoSuperiorDireito.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.troncoMacro.troncoSupMicro.troncoSuperiorEsquerdo.isSelected
                                    )
                                ),
                              )
                          ),

                          //TroL
                          Flexible(
                            flex: 4,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.troncoMacro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoSupMicro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoSupMicro.troncoSuperiorEsquerdo);
                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.troncoMacro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoSupMicro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoSupMicro.troncoSuperiorDireito);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.troncoMacro.troncoSupMicro.troncoSuperiorEsquerdo.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.troncoMacro.troncoSupMicro.troncoSuperiorDireito.isSelected
                                    )
                                ),
                              )
                          ),

                          //AntBraL
                          Flexible(
                            flex: 10,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 80.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoEsquerdo,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.antebracoEsquerdo);
                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoDireito,
                                              widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro,
                                              widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.antebracoDireito);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.antebracoEsquerdo.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.antebracoDireito.isSelected
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),



                    //Tronco inferior - Braço
                    Flexible(
                      flex: 12,
                      child: Row(
                        children: [

                          //braR
                          Flexible(
                              flex: 10,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(50.0, 0.0, 0.0, 0.0,),
                                child:  InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoDireito,
                                              widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro,
                                              widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.bracoDireito);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoEsquerdo,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.bracoEsquerdo);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.bracoDireito.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.bracoEsquerdo.isSelected
                                    )
                                ),
                              )
                          ),

                          //troInfR
                          Flexible(
                              flex: 4,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.troncoMacro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoInfMicro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoInfMicro.troncoInferiorDireito);
                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.troncoMacro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoInfMicro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoInfMicro.troncoInferiorEsquerdo);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.troncoMacro.troncoInfMicro.troncoInferiorDireito.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.troncoMacro.troncoInfMicro.troncoInferiorEsquerdo.isSelected
                                    )
                                ),
                              )
                          ),

                          //troInfL
                          Flexible(
                              flex: 4,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.troncoMacro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoInfMicro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoInfMicro.troncoInferiorEsquerdo);
                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.troncoMacro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoInfMicro,
                                              widget.controllerBodyPage.body.troncoMacro.troncoInfMicro.troncoInferiorDireito);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.troncoMacro.troncoInfMicro.troncoInferiorEsquerdo.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.troncoMacro.troncoInfMicro.troncoInferiorDireito.isSelected
                                    )
                                ),
                              )
                          ),

                          //braL
                          Flexible(
                              flex: 10,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 50.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoEsquerdo,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.bracoEsquerdo);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                          widget.controllerBodyPage.body.bracoDireito,
                                        widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro,
                                        widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.bracoDireito);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.bracoEsquerdo.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.bracoDireito.isSelected,
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),



                    //Coxa - Mão
                    Flexible(
                      flex: 18,
                      child: Row(
                        children: [

                          //MãoR
                          Flexible(
                              flex: 10,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(40.0, 0.0, 15.0, 40.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoDireito,
                                              widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro,
                                              widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.maoDireita);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoEsquerdo,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.maoEsquerda);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.maoDireita.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.maoEsquerda.isSelected
                                    )
                                ),
                              )
                          ),

                          //CoxaR
                          Flexible(
                              flex: 6,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(2.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaDireitaMacro,
                                              widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro,
                                              widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.coxaDireita);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.coxaEsquerda);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.coxaDireita.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.coxaEsquerda.isSelected
                                    )
                                ),
                              )
                          ),

                          //CoxaL
                          Flexible(
                              flex: 6,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 2.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.coxaEsquerda);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                          widget.controllerBodyPage.body.pernaDireitaMacro,
                                        widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro,
                                        widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.coxaDireita);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.coxaEsquerda.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.coxaDireita.isSelected
                                    )
                                ),
                              )
                          ),

                          //MãoL
                          Flexible(
                              flex: 10,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(15.0, 0.0, 40.0, 40.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.bracoEsquerdo,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro,
                                              widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.maoEsquerda);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                          widget.controllerBodyPage.body.bracoDireito,
                                        widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro,
                                        widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.maoDireita);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.bracoEsquerdo.bracoEsquerdoMicro.maoEsquerda.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.bracoDireito.bracoDireitoMicro.maoDireita.isSelected
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),



                    //Joelho
                    Flexible(
                      flex: 6,
                      child: Row(
                        children: [
                          //joeR
                          Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(130.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaDireitaMacro,
                                              widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro,
                                              widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.joelhoDireito);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.joelhoEsquerdo);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.joelhoDireito.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.joelhoEsquerdo.isSelected
                                    )
                                ),
                              )
                          ),

                          //JoeL
                          Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 130.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.joelhoEsquerdo);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                          widget.controllerBodyPage.body.pernaDireitaMacro,
                                        widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro,
                                        widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.joelhoDireito);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.joelhoEsquerdo.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.joelhoDireito.isSelected
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),



                    //Perna
                    Flexible(
                      flex: 12,
                      child: Row(
                        children: [

                          //PerR
                          Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(130.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaDireitaMacro,
                                              widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro,
                                              widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.pernaDireita);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.pernaEsquerda);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.pernaDireita.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.pernaEsquerda.isSelected
                                    )
                                ),
                              )
                          ),

                          //PerL
                          Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 130.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.pernaEsquerda);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                          widget.controllerBodyPage.body.pernaDireitaMacro,
                                        widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro,
                                        widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.pernaDireita);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.pernaEsquerda.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.pernaDireita.isSelected
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),



                    //Pe
                    Flexible(
                      flex: 10,
                      child: Row(
                        children: [

                          //PeR
                          Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(130.0, 0.0, 0.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaDireitaMacro,
                                              widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro,
                                              widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.peDireito);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.peEsquerdo);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.peDireito.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.peEsquerdo.isSelected
                                    )
                                ),
                              )
                          ),

                          //peL
                          Flexible(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(0.0, 0.0, 130.0, 0.0,),
                                child: InkWell(
                                    onTap: (){
                                      setState(() {
                                        if(widget.controllerBodyPage.frontal) {
                                          widget.controllerBodyPage
                                              .changeSelect(
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro,
                                              widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.peEsquerdo);

                                        }else{
                                          widget.controllerBodyPage
                                              .changeSelect(
                                          widget.controllerBodyPage.body.pernaDireitaMacro,
                                        widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro,
                                        widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.peDireito);
                                        }
                                      });
                                    },
                                    child:  BodySelectContainer(
                                        frontal: widget.controllerBodyPage.frontal,
                                        botaoFrontL: widget.controllerBodyPage.body.pernaEsquerdaMacro.pernaEsquerdaMicro.peEsquerdo.isSelected,
                                        botaoFrontR: widget.controllerBodyPage.body.pernaDireitaMacro.pernaDireitaMicro.peDireito.isSelected
                                    )
                                ),
                              )
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
        ),

        Text(
            widget.controllerBodyPage.getPartSelected(),
          style: const TextStyle(
              color: Colors.deepPurple,
              fontWeight: FontWeight.bold,
              fontSize: 20
          ),
        ),
      ],
    )

    );
  }



  Future<dynamic> showModalBottomSheetSelectTag(BuildContext context) {
    return showModalBottomSheet(
      //  isDismissible: false,
      //  enableDrag: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(24),
            topRight: Radius.circular(24),
          ),
        ),
        builder: (context) {
          return SingleChildScrollView(
            child: StatefulBuilder(
              builder: (BuildContext context, setState) => WillPopScope(
                onWillPop: () async {
                  Navigator.of(context).pop();
                  return false;
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 48.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const Padding(
                        padding: EdgeInsets.all(32.0),
                        child: Text(
                          'Preencha a descrição',
                          style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),

                      !widget.controllerBodyPage.listTags[0].isSelected
                      ? const TextField()
                      : Container(),

                      !widget.controllerBodyPage.listTags[1].isSelected
                          ? const TextField()
                          : Container(),

                      !widget.controllerBodyPage.listTags[2].isSelected
                          ? const TextField()
                          : Container(),


                      widget.controllerBodyPage.getLengthListTagSelected() < 3
                      ? DropdownButton(
                        items: widget.controllerBodyPage.listTags.map((Tags tag) {
                          return DropdownMenuItem<Tags>(
                            value: tag,

                            child: Text(tag.descricao),
                          );
                        }).toList(),
                        onChanged: (tag) {
                          setState(() {
                            widget.controllerBodyPage.addTag(tag as Tags);
                          });
                        },
                      )
                      : Container(),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          OutlinedButton(
                            onPressed: () async {

                              Navigator.of(context).pop();
                            },

                            child: const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text('Cancelar'),
                            ),
                          ),

                          OutlinedButton(
                            onPressed: () async {

                              Navigator.of(context).pop();

                            },


                            child:const Padding(
                              padding: EdgeInsets.all(8.0),
                              child: Text('Confirmar'),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}





class BodySelectContainer extends StatelessWidget {
  final seletionColor = const Color(0x7C009EFD);
  final seletionBorderColor = const Color(0xFF033F7B);

  const BodySelectContainer({
    Key? key,
    required this.frontal,
    required this.botaoFrontL,
    required this.botaoFrontR,

  }) : super(key: key);

  final bool frontal;
  final bool botaoFrontL;
  final bool botaoFrontR;


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration:
      frontal
      ? getBoxDecoration(botaoFrontL)
      : getBoxDecoration(botaoFrontR)
     );
  }


  BoxDecoration getBoxDecoration(bool botaoSelected) {
    return botaoSelected
        ? BoxDecoration(
        color: seletionColor,
        border: Border.all(color: seletionBorderColor))
        : const BoxDecoration(
      color: Colors.transparent,
    );
  }
}


BoxDecoration getBoxDecoration(bool botaoSelected) {
  const seletionColor = const Color(0x7C009EFD);
  const seletionBorderColor = const Color(0xFF033F7B);
  return botaoSelected
      ? BoxDecoration(
      color: seletionColor,
      border: Border.all(color: seletionBorderColor))
      : const BoxDecoration(
    color: Colors.transparent,
  );
}

