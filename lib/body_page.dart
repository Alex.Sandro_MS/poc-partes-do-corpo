import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// These are all of the selectable body parts.
enum BodyParts {
  leftArm, head, torso, leftLeg, rightLeg, rightArm
}

class BodyPage extends StatefulWidget {
  const BodyPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<BodyPage> createState() => _BodyPageState();
}

class _BodyPageState extends State<BodyPage> {
  BodyParts selected = BodyParts.head;
  /// This is the height of the entire body diagram.
  final double bodyHeight = 650.0;

  /// This is the height of the head.
  final double headHeight = 50.0;

  @override
  Widget build(BuildContext context) {
    return
      Container(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: SingleChildScrollView(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                // Left arm
                Column(
                  children: [
                    MaterialButton(
                      padding: const EdgeInsets.all(0),
                      minWidth: 0,
                      child: Image.asset(
                          selected == BodyParts.leftArm
                              ? "assets/images/left_arm_selected.png"
                              : "assets/images/left_arm.png"
                      ),
                      onPressed: () => setState(() => selected = BodyParts.leftArm),
                    ),

                    Container(
                      height: 100,
                    ),
                  ],
                ),


                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // Head
                    MaterialButton(
                      padding: const EdgeInsets.all(0),
                      minWidth: 0,
                      child: Image.asset(
                          selected == BodyParts.head
                              ? "assets/images/head_selected.png"
                              : "assets/images/head.png"
                      ),
                      onPressed: () => setState(() => selected = BodyParts.head),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [

                        // Torso
                        MaterialButton(
                          padding: const EdgeInsets.all(0),
                          minWidth: 0,
                          child: Image.asset(
                              selected == BodyParts.torso
                                  ? "assets/images/torso_selected.png"
                                  : "assets/images/torso.png"
                          ),
                          onPressed: () => setState(() => selected = BodyParts.torso),
                        ),
                      ],
                    ),

                    // Legs
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                        // Left leg
                          MaterialButton(
                            padding: const EdgeInsets.all(0),
                            minWidth: 0,
                            child: Image.asset(
                                selected == BodyParts.leftLeg
                                    ? "assets/images/left_leg_selected.png"
                                    : "assets/images/left_leg.png"
                            ),
                            onPressed: () => setState(() => selected = BodyParts.leftLeg),
                          ),

                        // Right leg
                          MaterialButton(
                            padding: const EdgeInsets.all(0),
                            minWidth: 0,
                            child: Image.asset(
                                selected == BodyParts.rightLeg
                                    ? "assets/images/right_leg_selected.png"
                                    : "assets/images/right_leg.png"
                            ),
                            onPressed: () => setState(() => selected = BodyParts.rightLeg),
                          ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
                // Right arm
                Column(
                  children: [
                    MaterialButton(
                      padding: const EdgeInsets.all(0),
                      minWidth: 0,
                      child: Image.asset(
                          selected == BodyParts.rightArm
                              ? "assets/images/right_arm_selected.png"
                              : "assets/images/right_arm.png"
                      ),
                      onPressed: () => setState(() => selected = BodyParts.rightArm),
                    ),
                    Container(
                      height: 100,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
  }
}